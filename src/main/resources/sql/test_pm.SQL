# SQL SCRIPT FOR database test_pm.

DROP DATABASE IF EXISTS `test_pm`;
CREATE DATABASE IF NOT EXISTS `test_pm`;
DROP TABLE IF EXISTS .`test_pm`.`id_counter`;

CREATE TABLE `test_pm`.`id_counter` (
   `project_id` varchar(45) NOT NULL,
   `counter` varchar(10000) NOT NULL,
   PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP USER IF EXISTS 'testpm';
CREATE USER 'testpm' IDENTIFIED BY 'testpm';
GRANT ALL PRIVILEGES ON test_pm.* TO 'testpm';