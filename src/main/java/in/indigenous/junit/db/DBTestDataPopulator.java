package in.indigenous.junit.db;

public interface DBTestDataPopulator {
	
	void generate(final String configXML, final String dataXML);

}
