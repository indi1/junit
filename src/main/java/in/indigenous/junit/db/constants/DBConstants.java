package in.indigenous.junit.db.constants;

public final class DBConstants {
	
	private DBConstants() {
		// No Instantiation
	}
	
	public final static String XSD_PATH = "http://www.indigenous.in/public/xsd/database-info-v0.0.1.xsd";
	
}
