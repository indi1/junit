package in.indigenous.junit.db;

import in.indigenous.junit.jaxb.gen.Database;
import in.indigenous.util.db.model.Configuration;

public interface DBConfigurationPreparor {
	
	Configuration getConfiguration(Database database);
	
	Configuration getTestConfiguration(Database database);

}
