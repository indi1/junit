package in.indigenous.junit.db.test;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ContextConfiguration(locations= {"file:src/main/resources/META-INF/config/*-context.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AbstractIT {
	
	private static ApplicationContext context;
	
	@BeforeClass
	public static void setup() {
		context = new ClassPathXmlApplicationContext("file:src/main/resources/META-INF/config/*-context.xml");
	}

}
