package in.indigenous.junit.db.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import in.indigenous.junit.db.DBTestDataPopulator;

@RunWith(Suite.class)
public class AbstractITSuite {

	private static ApplicationContext context;

	private static DBTestDataPopulator defaultDbTestDataPopulator;

	private static boolean setupDone = false;

	public static void setUp(final String xmlConfig, final String xmlData) {
		if (!setupDone) {
			try {
				context = new ClassPathXmlApplicationContext("file:src/main/resources/META-INF/config/*-context.xml");
				defaultDbTestDataPopulator = (DBTestDataPopulator) context.getBean("defaultDbTestDataPopulator");
				defaultDbTestDataPopulator.generate(xmlConfig, xmlData);
				setupDone = true;
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}
	}

	public static void tearDown() {
		setupDone = false;
	}
}
