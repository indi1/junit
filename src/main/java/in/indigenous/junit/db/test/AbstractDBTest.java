package in.indigenous.junit.db.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ContextConfiguration(locations= {"file:src/main/resources/META-INF/config/*-context.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AbstractDBTest {
	
	
}
