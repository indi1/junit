package in.indigenous.junit.db.impl;

import java.util.Map;

import org.apache.commons.lang3.BooleanUtils;

import in.indigenous.junit.db.DBConfigurationPreparor;
import in.indigenous.junit.jaxb.gen.Database;
import in.indigenous.util.db.constants.DatabaseType;
import in.indigenous.util.db.model.Configuration;
import in.indigenous.util.db.model.DatabaseParams;
import in.indigenous.utility.db.ConnectionUrlPreparor;

public class DefaultDBConfigurationPreparor implements DBConfigurationPreparor {

	private Map<DatabaseType, ConnectionUrlPreparor> connectionUrlPreparorMap;

	private Map<DatabaseType, String> driverClassMap;

	@Override
	public Configuration getConfiguration(Database database) {
		Configuration conf = createDefault(database);
		conf.setDatabaseName(database.getDatabaseName());
		conf.setConnectionUrl(getConnectionUrl(database.getDatabaseType(), database.getDatabaseServer(),
				database.getPort().toString(), database.getDatabaseName()));
		return conf;
	}

	@Override
	public Configuration getTestConfiguration(final Database database) {
		Configuration conf = createDefault(database);
		conf.setConnectionUrl(getConnectionUrl(database.getDatabaseType(), database.getDatabaseServer(),
				database.getPort().toString(), database.getDatabaseName()));
		String dbName = new StringBuilder("test_").append(database.getDatabaseName()).toString();
		conf.setTestDatabaseName(dbName);
		conf.setTestConnectionUrl(getConnectionUrl(database.getDatabaseType(), database.getDatabaseServer(),
				database.getPort().toString(), dbName));
		conf.setTestUser(database.getTestUser());
		conf.setTestPassword(database.getTestPassword());
		return conf;
	}

	private String getDriverClass(final String databaseType) {
		return driverClassMap.get(DatabaseType.valueOf(databaseType));
	}

	private String getConnectionUrl(final String databaseType, final String server, final String port,
			final String dbName) {
		DatabaseParams params = new DatabaseParams();
		params.setDbName(dbName);
		params.setPort(port);
		params.setServer(server);
		return connectionUrlPreparorMap.get(DatabaseType.valueOf(databaseType)).getConnectionUrl(params);
	}

	private Configuration createDefault(final Database database) {
		Configuration conf = new Configuration();
		if (BooleanUtils.toBoolean(database.getSchema())) {
			conf.setScript(database.getSchemaPath());
		}
		conf.setDatabaseType(DatabaseType.valueOf(database.getDatabaseType()));
		conf.setDriver(getDriverClass(database.getDatabaseType()));
		conf.setUser(database.getDatabaseUser());
		conf.setPassword(database.getDatabasePassword());
		return conf;
	}

	public void setConnectionUrlPreparorMap(Map<DatabaseType, ConnectionUrlPreparor> connectionUrlPreparorMap) {
		this.connectionUrlPreparorMap = connectionUrlPreparorMap;
	}

	public void setDriverClassMap(Map<DatabaseType, String> driverClassMap) {
		this.driverClassMap = driverClassMap;
	}

}
