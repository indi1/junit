package in.indigenous.junit.db.impl;

import org.springframework.beans.factory.annotation.Autowired;

import in.indigenous.junit.db.DBConfigurationPreparor;
import in.indigenous.junit.db.DBTestDataPopulator;
import in.indigenous.junit.db.constants.DBConstants;
import in.indigenous.junit.jaxb.gen.Database;
import in.indigenous.util.db.constants.DataFileFormat;
import in.indigenous.util.db.constants.DatabaseType;
import in.indigenous.util.db.model.Configuration;
import in.indigenous.utility.db.DatabaseScriptGenerator;
import in.indigenous.utility.db.TestDataPopulator;
import in.indigenous.utility.db.TestDatabaseScriptExecutor;
import in.indigenous.utility.log.IndigenousLogger;
import in.indigenous.utility.service.ServiceLocator;
import in.indigenous.utility.xml.XMLUtil;

public class DefaultDBTestDataPopulator implements DBTestDataPopulator {

	@Autowired
	private XMLUtil defaultXMLUtil;

	@Autowired
	private DBConfigurationPreparor defaultDBConfigurationPreparor;

	@Autowired
	private DatabaseScriptGenerator mysqlDatabaseScriptGenerator;

	@Autowired
	private ServiceLocator<DatabaseType, TestDataPopulator> testDataPopulatorLocator;

	@Autowired
	private ServiceLocator<DatabaseType, TestDatabaseScriptExecutor> testDatabaseScriptExecutorLocator;

	private static final IndigenousLogger LOG = IndigenousLogger.getInstance(DefaultDBTestDataPopulator.class);

	@Override
	public void generate(final String configXML, final String dataXML) {

		if (validateXML(DBConstants.XSD_PATH, configXML)) {
			Database database = defaultXMLUtil.generate(Database.class, configXML);
			Configuration conf = defaultDBConfigurationPreparor.getConfiguration(database);
			mysqlDatabaseScriptGenerator.generate(conf);
			Configuration testConf = defaultDBConfigurationPreparor.getTestConfiguration(database);
			testConf.setDatabaseMetadata(conf.getDatabaseMetadata());
			mysqlDatabaseScriptGenerator.generateTest(testConf);
			testConf.setDataFile(dataXML);
			testConf.setDataFormat(DataFileFormat.XML);
			loadData(testConf);
		} else {
			LOG.error("Error validating xml file.", new Exception());
		}
	}

	private boolean validateXML(final String xsdPath, final String xmlPath) {
		return defaultXMLUtil.validateXML(xsdPath, xmlPath);
	}

	private void loadData(final Configuration conf) {
		if(testDatabaseScriptExecutorLocator == null) {
			System.err.println("testDatabaseScriptExecutorLocator null");
		} else if(testDatabaseScriptExecutorLocator.locate(conf.getDatabaseType()) == null) {
			System.err.println("Could not retrieve testDatabaseScriptExecutor for key " + conf.getDatabaseType());
		}
		testDatabaseScriptExecutorLocator.locate(conf.getDatabaseType()).execute(conf);
		testDataPopulatorLocator.locate(conf.getDatabaseType()).populate(conf);
	}

}
