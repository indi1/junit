package in.indigenous.junit.db;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import in.indigenous.junit.db.impl.DefaultDBTestDataPopulatorIT;
import in.indigenous.junit.db.test.AbstractITSuite;

@RunWith(Suite.class)
@SuiteClasses({DefaultDBTestDataPopulatorIT.class})
public class AllTestsIT extends AbstractITSuite {

}
