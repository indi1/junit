package in.indigenous.junit.db.impl;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import in.indigenous.junit.db.DBTestDataPopulator;
import in.indigenous.junit.db.test.AbstractIT;
import in.indigenous.utility.boot.Registry;

public class DefaultDBTestDataPopulatorIT extends AbstractIT {

	@Autowired
	private DBTestDataPopulator defaultDbTestDataPopulator;

	@Before
	public void setUp() {
		// AbstractIT.setup();
		defaultDbTestDataPopulator = (DBTestDataPopulator) Registry.getApplicationContext()
				.getBean("defaultDbTestDataPopulator");
		if (defaultDbTestDataPopulator == null) {
			System.err.println("No bean defaultDbTestDataPopulator found.");
		} else {
			System.out.println("defaultDbTestDataPopulator == " + defaultDbTestDataPopulator);
		}
	}

	// @Test
	public void testGenerateWithScript() {
		defaultDbTestDataPopulator.generate("./src/main/resources/xml/test/database-info-script.xml",
				"./src/main/resources/xml/test/test-data.xml");
	}

	@Test
	public void testGenerateWithoutScript() {
		defaultDbTestDataPopulator = (DBTestDataPopulator) Registry.getApplicationContext()
				.getBean("defaultDbTestDataPopulator");
		try {
			defaultDbTestDataPopulator.generate("./src/main/resources/xml/test/database-info.xml",
					"./src/main/resources/xml/test/test-data.xml");
		} catch (Throwable t) {
			t.printStackTrace();
			fail();
		}
	}

}
